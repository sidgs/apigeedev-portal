<?php

/**
 * @file
 * Lists available colors and color schemes for the Bartik theme.
 */

$info = [
  // Available colors and color labels used in theme.
  'fields' => [
    'primary_nav_bg' => t('Primary Nav Bg'),
    'header' => t('Header background'),
    'header_content' => t('Header Content'),
    'bg' => t('Main background'),
    'sidebar' => t('Sidebar background'),
    'sidebarborders' => t('Sidebar borders'),
    'footer' => t('Footer background'),
    'titleslogan' => t('Title and slogan'),
    'text' => t('Text color'),
    'link' => t('Link color'),
    'link_hover' => t('Link hover')
  ],
  // Pre-defined color schemes.
  'schemes' => [
    'default' => [
      'title' => t('Iris (default)'),
      'colors' => [
        'primary_nav_bg' => '#ee4723',
        'header' => '#084bce',
        'header_content' => '#fdfdfd',
        'bg' => '#ffffff',
        'sidebar' => '#ffffff',
        'sidebarborders' => '#e9ecef',
        'footer' => '#212529',
        'titleslogan' => '#fffeff',
        'text' => '#32383e',
        'link' => '#0071b3',
        'link_hover' => '#0071b4',
      ],
    ],
  ],

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => [
    'css/colors.css',
  ],

  // Files to copy.

  // Gradient definitions.
  'gradients' => [
    [
      // (x, y, width, height).
      'dimension' => [0, 0, 0, 0],
      // Direction of gradient ('vertical' or 'horizontal').
      'direction' => 'vertical',
      // Keys of colors to use for the gradient.
      'colors' => ['top', 'bottom'],
    ],
  ],

  // Preview files.
  'preview_library' => 'iris/color.preview',
  'preview_html' => 'color/preview.html',

  // Attachments.
  '#attached' => [
    'drupalSettings' => [
      'color' => [
        // Put the logo path into JavaScript for the live preview.
      ],
    ],
  ],
];
